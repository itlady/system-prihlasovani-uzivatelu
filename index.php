<?php 
    require "vendor/autoload.php";
    include "db.php";
    include "head.php";
    
?>

    <div class="container col-4 col-lg-3">
    <?php if(isset($_GET['added'])){?>
            <div class="alert alert-danger col-12 mx-auto my-3 text-center">Registrace nového uživatele proběhla úspěšně</div>
    <?php } ?>
    <?php if(isset($_GET['logout'])){?>
            <div class="alert alert-danger col-12 mx-auto my-3 text-center">Uživatel byl úspěšně odhlášen</div>
    <?php } ?>

        <div class="mt-5">
            <div class="row align-items-center">
                <a href="view.php" class="mb-3 btn btn-warning btn-lg" role="button">Seznam uživatelů</a>
                <a href="add.php" class="mb-3 btn btn-warning btn-lg" role="button">Nový uživatel</a>
                <a href="login.php" class="btn btn-warning btn-lg" role="button">Přihlásit uživatele</a>
            </div>
        </div>
    </div>

<?php include "footer.php";?>
