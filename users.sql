-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Úte 05. dub 2022, 14:53
-- Verze serveru: 10.4.22-MariaDB
-- Verze PHP: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `vos_login`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `jmeno` varchar(30) COLLATE utf8mb4_czech_ci NOT NULL,
  `prijmeni` varchar(45) COLLATE utf8mb4_czech_ci NOT NULL,
  `nick` text COLLATE utf8mb4_czech_ci NOT NULL,
  `heslo` text COLLATE utf8mb4_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `jmeno`, `prijmeni`, `nick`, `heslo`) VALUES
(18, 'Bob', 'Bobek', 'rabbit1', '$2y$10$82PmQqexvB4AdjjZVLD7Mu22bYDcXWaV445Mjq9IyhzYkdyBRyX/.'),
(19, 'Admin', 'Administrátor', 'admin', '$2y$10$MJjfoaRYadH93oarnxdkqungokATLbdUiLCtGtbkefCyqiAbZbfNm'),
(20, 'Štěpán', 'Zemánek', 'zemast', '$2y$10$6q.m9EH63HAZ/Vt1MfrLje1E34fbsbbrtP9h33lewA/BaEtylUoC2'),
(21, 'Eliška', 'Kudrnová', 'elis', '$2y$10$xR5/Ah.Ee2ncg7.eHB0Fwe8uKdXFlXWaljG2UIPUD18iS21jGQRBG'),
(22, 'Adam', 'Novák', 'adams', '$2y$10$UHd/dr82fyrNe/w4dnKmJOGpVogMc0GQBcekWMnD.UaXg532azb2m'),
(24, 'Eva', 'Novotná', 'evka', '$2y$10$Xq9NVqtrYS/cfYxplv09GeivBdl5O3SYSFDNT2g1qHCHUPutxgxxS'),
(25, 'Ivan', 'Paclt', 'kuře', '$2y$10$rLUYtvTHA51HpO.RG30eBOASwbGBToon80FFOzgmo3Bixv9EnDzei'),
(26, 'Jana', 'Šimková', 'břízka', '$2y$10$HTmD9xVOoxXZM5CxL8OhdOluUs92/LaINYJZMMk8Wx5jpXbMQSLia'),
(27, 'Tatka', 'Šmoula', 'tatka', '$2y$10$Hfc22qUmFHisG6yamEUQG.OXatnCGAonpc/JAB2/19jkBdVR.nzJu'),
(28, 'Matěj', 'Votava', 'mates', '$2y$10$wYk.yOm2Q5wE.x/ayNOFxOdKZchnzJ58v5eoqEHxrQD6fTev8CWUK');

--
-- Indexy pro exportované tabulky
--

--
-- Indexy pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
