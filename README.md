# Přihlašování uživatelů v PHP

## VOSPlzeň, Webové technologie, 3. semestr
---
### Systém uživatelů umožňuje:

* zobrazit seznam uložených uživatelů
* vložit nového uživatele
* libovolného uživatele přihlásit pomocí jména a hesla
* přihlášeného uživatele odhlásit
* export uživatelů do xlsx 
