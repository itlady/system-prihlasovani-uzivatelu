<?php 
    session_start();
    include "db.php";
    include "head.php";

    $prihlasen = $_SESSION["prihlasen"];

    if($prihlasen) {
        $id = $_GET["id"];
        $sql = "SELECT * FROM users WHERE id = '$id'";
        $query = mysqli_query($conn, $sql);
        $result = mysqli_fetch_array($query);
    } else {
        $prihlasen = false;
        header("Location: index.php?logout");
    } 

    if(isset($_POST["nodhlasit"])) {
        $_SESSION = array();
        session_destroy();
        header("Location: index.php?logout");
        exit();
    }
?>

    <div class="container col-10 col-lg-6">
        <h2 class="bg-warning bg-gradient rounded text-center mt-5 py-2">
            Uživatel přihlášen jako: <?php echo $result["nick"] ;?>
        </h2>
        <div class="alert alert-success my-5 col-6 text-center mx-auto"> Zatím neexistuje žádný obsah </div>
        <form action="private.php" method="post" class="my-5 text-center">
            <input name="nodhlasit" type="submit" value="Odhlásit" class="px-4 py-2 rounded" style="font-size: 1rem;">
        </form>
    </div>

<?php include "footer.php";?>
