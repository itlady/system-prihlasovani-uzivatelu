<?php 
    session_start();
    include "db.php";
    include "head.php";

    $msg = "Chybné uživatelské jméno nebo heslo";
    $_SESSION["prihlasen"] = false;

    if(isset($_POST["nlogin"])){
        $nick = htmlspecialchars($_POST["nnick"]);
        $heslo = $_POST["nheslo"];
        $sql = "SELECT * FROM users WHERE nick = '$nick'";
        $query = mysqli_query($conn, $sql);
        $result = mysqli_fetch_assoc($query);
    
        if(password_verify($heslo, $result["heslo"])){
            $_SESSION["prihlasen"] = true;
            $_SESSION["id"] = $result["id"];
            $id = $_SESSION["id"];
            header("Location:private.php?id=$id");
        } else {
            header("Location:login.php?badlogin");
            $_SESSION["prihlasen"] = false;
         }
    } else {
        $_SESSION["prihlasen"] = false;
    }
?>

<div class="container col-10 col-lg-6">
        <h2 class="bg-warning bg-gradient rounded text-center mt-5 py-2">Přihlásit uživatele</h2>

        <?php if(isset($_GET['badlogin'])){?>
            <div class="alert alert-danger col-6 mx-auto my-3 text-center"><?php echo $msg; ?></div>
        <?php } ?>

        <form action="login.php" method="post" class="mx-auto mt-5">
            <div class="mb-3 mx-auto" style="width: 280px;">
                <label class="form-label">Přezdívka:</label>
                <input name="nnick" type="text" class="form-control" placeholder="nickname" required>
            </div>
            <div class="mb-3 mx-auto" style="width: 280px;">
                <label class="form-label">Heslo:</label>
                <input name="nheslo" type="password" class="form-control" placeholder="heslo" required>
            </div>
            <div class="text-center my-5">
                <a href="index.php"><input type="button" class="btn btn-primary" value="Zpět" style="width: 100px;"><a>
                <button name="nlogin" type="submit" class="btn btn-primary ms-2" style="width: 120px;">Přihlásit</button>
            </div>
        </form>
    </div>

<?php include "footer.php";?>
