<?php 
    include "makexlsx.php";
    include "db.php";
    include "head.php";

?>
    <div class="container col-10 col-lg-6">
        <h2 class="bg-warning rounded text-center mt-5 py-2">Seznam všech uživatelů v databázi</h2>
        <div class="mt-5 col-8 mx-auto" >
      
        <table class="table table-info table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">jméno</th>
                    <th scope="col">příjmení</th>
                    <th scope="col">přezdívka</th>
                </tr>
            </thead>
            <?php foreach($query1 as $q) { ?>
                <tbody>
                    <tr>
                        <th><?php echo $q["id"]; ?></th>
                        <td><?php echo $q["jmeno"]; ?></td>
                        <td><?php echo $q["prijmeni"]; ?></td>
                        <td><?php echo $q["nick"]; ?></td>
                    </tr>
                </tbody>
            <?php } ?>
        </table>
    
        <div class="text-center">
            <form action="view.php" method="post">
                <a href="index.php" class="btn btn-primary mb-5 mt-4" style="width: 120px;">Zpět</a>
                <input type="submit" name="nexport" class="btn btn-success mb-5 mt-4 ms-3" style="width: 120px;" value="Export">
            </form>
        </div>
    </div>

<?php include "footer.php";?>
