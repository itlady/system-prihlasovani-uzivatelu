<?php 
    include "db.php";
    include "head.php";
  
    $missing = "Je třeba vyplnit všechna pole";
    $heslo_znovu = "Chyba při ověřování hesla";
    $duplicate = "Uživatel s touto přezdívkou již existuje";

    if(isset($_POST["npridat"])){
        if($_POST["nheslo"] != $_POST["nheslo_znovu"]) {
            header("Location: add.php?error");
            exit();
        } else {
            $jmeno = htmlspecialchars($_POST["njmeno"]);
            $prijmeni = htmlspecialchars($_POST["nprijmeni"]);
            $nick = htmlspecialchars($_POST["nnick"]);
            $heslo = password_hash($_POST["nheslo"], PASSWORD_BCRYPT);
            
            $sql = "SELECT * FROM users WHERE nick = '$nick'";
            $query = mysqli_query($conn,$sql);
            $result = mysqli_fetch_array($query);

            if($result["nick"] === $nick){
                header("Location:add.php?duplicate");
            } else {
                if(!empty($jmeno) && !empty($prijmeni) && !empty($nick) && !empty($heslo)) {
                    $sql = "INSERT INTO users(jmeno, prijmeni, nick, heslo) VALUES ('$jmeno', '$prijmeni', '$nick', '$heslo')";
                    mysqli_query($conn, $sql);
                    header("Location:index.php?added");
                    exit();
                } else {
                    header("Location:add.php?missing");
                }
            }
        }
    }

?>

    <div class="container col-10 col-lg-6">
        <h2 class="bg-warning bg-gradient rounded text-center mt-5 py-2">Vytvořit nového uživatele</h2>

        <?php if(isset($_GET['error'])){?>
            <div class="alert alert-danger col-6 mx-auto my-3 text-center"><?php echo $heslo_znovu; ?></div>
        <?php } ?>

        <?php if(isset($_GET['missing'])){?>
            <div class="alert alert-danger col-6 mx-auto my-3 text-center"><?php echo $missing; ?></div>
        <?php } ?>

        <?php if(isset($_GET['duplicate'])){?>
            <div class="alert alert-danger col-6 mx-auto my-3 text-center"><?php echo $duplicate; ?></div>
        <?php } ?>

        <form action="add.php" method="post" class="mx-auto mt-5">
            <div class="mb-3 mx-auto" style="width: 280px;">
                <label class="form-label">Jméno:</label>
                <input name="njmeno" type="text" class="form-control" placeholder="jméno" required>
            </div>
            <div class="mb-3 mx-auto" style="width: 280px;">
                <label class="form-label">Příjmení:</label>
                <input name="nprijmeni" type="text" class="form-control" placeholder="přijmení" required>
            </div>
            <div class="mb-3 mx-auto" style="width: 280px;">
                <label class="form-label">Přezdívka:</label>
                <input name="nnick" type="text" class="form-control" placeholder="nickname" required>
            </div>
            <div class="mb-3 mx-auto" style="width: 280px;">
                <label class="form-label">Heslo:</label>
                <input name="nheslo" type="password" class="form-control" minlength="8" placeholder="heslo min 8 znaků" required>
            </div>
            <div class="mb-3 mx-auto" style="width: 280px;">
                <label class="form-label">Heslo:</label>
                <input name="nheslo_znovu" type="password" class="form-control" minlength="8" placeholder="heslo znovu" required>
            </div>
            <div class="text-center my-5">
                <a href="index.php"><input type="button" class="btn btn-primary" value="Zpět" style="width: 100px;"><a>
                <button name="npridat" type="submit" class="btn btn-primary ms-2" style="width: 120px;">Registrovat</button>
            </div>
        </form>
    </div>

<?php include "footer.php";?>