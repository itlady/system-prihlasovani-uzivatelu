<?php 
    require "vendor/autoload.php";
    include "db.php";
    
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    use PhpOffice\PhpSpreadsheet\Style\Fill;
    
    $filename = "output.xlsx";
    $sql = "SELECT id, jmeno, prijmeni, nick FROM users ORDER BY prijmeni ASC";
    $query= mysqli_query($conn,$sql);
        
    if(isset($_POST["nexport"])) {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // formát tabulky
        // šířka sloupců
        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(35);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        
        // nastavení fontu
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setSize(14);
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true);

        // styly
        $hlavicka = [
            "font" => [
                "color" => [
                    "rgb" => "111111"
                ],
                "bold" => true,
                "size"=> 14
            ],
            "fill" => [
                "fillType" => Fill::FILL_SOLID,
                "startColor" => [
                    "rgb" => "339af0"
                ]
            ]
        ];

        //liché řádky
        $lichy = [
            "font" => [
                "color" => [
                    "rgb" => "111111"
                ],
                "bold" => true,
                "size"=> 14
            ],
            "fill" => [
                "fillType" => Fill::FILL_SOLID,
                "startColor" => [
                    "rgb" => "74c0fc"
                ]
            ]
        ];

        $sudy = [
            "font" => [
                "color" => [
                    "rgb" => "111111"
                ],
                "bold" => true,
                "size"=> 14
            ],
            "fill" => [
                "fillType" => Fill::FILL_SOLID,
                "startColor" => [
                    "rgb" => "a5d8ff"
                ]
            ]
        ];

        // barva pozadí
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->applyFromArray($hlavicka);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->applyFromArray($hlavicka);

        // hlavička tabulky
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A1', "id")
                    ->setCellValue('B1', "jméno")
                    ->setCellValue('C1', "příjmení")
                    ->setCellValue('D1', "přezdívka");
        
        //data se plní od řádku 2
        $row = 2;
        
        //naplnění tabulky daty z db
        while($result = mysqli_fetch_row($query)) {
           $spreadsheet->getActiveSheet()
                ->setCellValue("A".$row, $result[0])
                ->setCellValue("B".$row, $result[1])
                ->setCellValue("C".$row, $result[2])
                ->setCellValue("D".$row, $result[3]);

                //střídání barev
                if($row % 2 == 0) {
                    $spreadsheet->getActiveSheet()->getStyle("A".$row.":D".$row)->applyFromArray($sudy);
                }else {
                    $spreadsheet->getActiveSheet()->getStyle("A".$row.":D".$row)->applyFromArray($lichy);
                }
                $row++;
                
            }
            $writer = new Xlsx($spreadsheet);
            $writer->save($filename);
           
        // možnost uložení souboru    
        echo "<meta http-equiv='refresh' content='0;url=$filename'/>";
   
    }

